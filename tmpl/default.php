<?php
/* 
 * mod_flexpopup default template
 * @package     Joomla.Site
 * @subpackage  mod_flexpopup
 * @date       2016
 * @author     Pierre Corell
 * @homepage   https://joomla-praxis.de
 * @license    LGPLv3
 * 
 * Flexpopup module creates flexible popups based on timing, cookies, wysiwyg or Joomla! contents
 */

// No direct access
defined('_JEXEC') or die;

if($params->get('loadJquery') === '1')
    JHtml::_('jquery.framework');

$buttonText = $params->get('openbutton');
$buttonClass = $params->get('buttonClass');

if($params->get('flexcontent') !== NULL ) {
    echo '<div class="flexContent">'
        .$params->get('flexcontent')
        .'<a href="#flex-'.$moduleid.'" data-target="'.$moduleid.'" data-close="'.$closePop.'" class="'.$buttonClass.'" id="modal-'.$moduleid.'">'
        .$buttonText.'</a>'
    . '</div>';
}
if ($popupContent) {
    $params->get('hideClose') === '1' ? $hidden = 'style="display:none"' : $hidden = '';
    echo '<div id="flex-'.$moduleid.'" data-top="'.$params->get('top').'" style="display:none;" class="flexPop">'
        . '<div id="flexContent" style="width:'.$flexWidth.'; background-color:'.$bgColor.'; top:'.$flexTop.';">'
            .$popupContent
            .'<a href="#" id="flexClose" '.$hidden.'>X</a>'
            . '</div>'
        . '</div>';
}
