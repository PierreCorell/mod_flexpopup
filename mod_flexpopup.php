<?php
/* 
 * Flexible Popup for Joomla! mod_flexpopup
 * @package     Joomla.Site
 * @subpackage  mod_flexpopup
 * @date       2016
 * @author     Pierre Corell
 * @homepage   https://joomla-praxis.de
 * @license    LGPLv3
 *  
 * Flexpopup module creates flexible popups based on timing, cookies, wysiwyg or Joomla! contents
 */
// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
$doc = JFactory::getDocument(); 
$doc->addScript( JURI::Root(true).'/modules/mod_flexpopup/js/flexpopup.min.js' );
$doc->addStylesheet ( JURI::Root(true).'/modules/mod_flexpopup/css/flexpopup.css' );
$doc->addScript( JURI::Root(true).'/modules/mod_flexpopup/js/mdzr-video_audio.js' );

$moduleId   = 'flexPop-'.$module->id; 
$moduleid   = $module->id; 
$bgColor = $params->get('bgColor');
$flexWidth = $params->get('width');
$flexTop = $params->get('top');

$lifetime   = $params->get('lifetime');
$cookieHelper = modFlexPopupHelper::getCookieModule($moduleId,$lifetime);
if($cookieHelper !== false) {
    $open = false;
}
else {
    $open = true;
}
if( $params->get('mode') === '1' ) {
    $popupContent = modFlexPopupHelper::getJoomlaArticles($params->get('artid'));
}
elseif( $params->get('mode') === '0' ) {
    $popupContent = $params->get('flexeditor');
}
if($params->get('cssStyle')) {
    $style = $params->get('cssStyle');
    $doc->addStyleDeclaration($style);
}
$openPop = $params->get('opentime')*1000;
$closePop = 0;
if( is_numeric($params->get('closetime')) && $params->get('closetime') > 0 ) {
    $closePop = $openPop + $params->get('closetime')*1000;
    $doc->addScriptDeclaration( '
        jQuery( document ).ready(function(){
            autoCloseFlexPopup("'.$closePop.'");
        });
    ' );
}
if($open === true ) {
    $doc->addScriptDeclaration( '
    jQuery( document ).ready(function(){
        window.setTimeout(
            function(){
                jQuery(".flexContent a").click(); 
            },'.$openPop.'
        );
        });
    ' );
}

require JModuleHelper::getLayoutPath('mod_flexpopup');
