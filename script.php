<?php
/* 
 * mod_flexpopup installer class
 * @package     Joomla.Site
 * @subpackage  mod_flexpopup
 * @date       2016
 * @author     Pierre Corell
 * @homepage   https://joomla-praxis.de
 * @license    LGPLv3
 */
// No direct access to this file
defined('_JEXEC') or die;
 
/**
 * Script file of FlexPopup module
 */
class mod_flexPopupInstallerScript
{
    /**
     * Method to install the extension
     * $parent is the class calling this method
     *
     * @return void
     */
    function install($parent) 
    {
        echo '<div class="alert alert-success">flexpopup module was successfully installed.</div>';
    }

    /**
     * Method to uninstall the extension
     * $parent is the class calling this method
     *
     * @return void
     */
    function uninstall($parent) 
    {
        echo '<div class="alert alert-info">Flexpopup module was successfully uninstalled.</div>';
    }

    /**
     * Method to update the extension
     * $parent is the class calling this method
     *
     * @return void
     */
    function update($parent) 
    {
        echo '<div class="alert alert-success">Thanks for updating flexpopup module to version '. $parent->get('manifest')->version . '</div>';
    }
}