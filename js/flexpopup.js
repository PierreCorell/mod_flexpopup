/* 
 * flexpopup module js file
 * @package    Joomla
 * @subpackage Modules
 * @date       2016
 * @version    2.2
 * @since      2.0
 * @author     Pierre Corell
 * @homepage   https://joomla-praxis.de
 */
function closeFlexPopup() {
    jQuery('#flexClose').click();
}
var timer;
function autoCloseFlexPopup(delay) {
    window.clearTimeout(timer);
    function closePopup() {
        jQuery('#flexClose').click();
    }
    timer = window.setTimeout(closePopup, delay);
}

jQuery( document ).ready(function() {
    jQuery('.flexContent a').click(function (e){
        e.preventDefault();
        var delay = jQuery(this).attr('data-close');
        initflexpopup('#flex-' + jQuery(this).attr('data-target'),0);
        autoCloseFlexPopup(delay);
    });
    jQuery('#flexClose').click(function (e) {
        e.preventDefault();
        jQuery('.flexPop').fadeOut(1000);
        jQuery('#flexContent').detach();
    });
    var flexpopup = jQuery("#flexContent");
    jQuery(flexpopup).detach();

    function initflexpopup(moduleid,delay) {
        function openFlex(){
            jQuery(moduleid).append(flexpopup);
            var video = jQuery("video", jQuery(this).html());
            var audio = jQuery("audio", jQuery(this).html());
            var media = false;
            if (video !== undefined && Modernizr.video !== false) media = video;
            else if(audio !== undefined && Modernizr.audio !== false) media = audio;
            if(media !== false && media.length === 1 && media.attr('autoplay') === 'autoplay') {
                media.get(0).play();
            }
            jQuery(moduleid).fadeIn(500).css( {
                'position'          : 'fixed',
                'z-index'           : 99,
                'backgroundColor'   : 'rgba(0,0,0,.3)',
                'height'            : '100%',
                'width'             : '100%',
                'left'              : 0,
                'top'               : 0
              });
        }
        setTimeout(openFlex, delay);
    }
});
