<?php
/**
 * Helper class for mod_flexpopup
 * @package     Joomla.Site
 * @subpackage  mod_flexpopup
 * @date       2016
 * @author     Pierre Corell
 * @homepage   https://joomla-praxis.de
 * @license    LGPLv3
 * 
 *  Flexpopup module creates flexible popups based on timing, cookies, wysiwyg or Joomla! contents
 */
// No direct access
defined('_JEXEC') or die;

class ModFlexPopupHelper
{   
    /**
     * get Joomla! article as defined per user
     * @param type $params
     * @return type
     */
    public static function getJoomlaArticles($params) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select($db->quoteName('introtext'))
            ->from($db->quoteName('#__content'))
            ->where('id = ' . $db->Quote($params))
            ->where('state=1');
        // Prepare the query
        $db->setQuery($query);
        // Load the row.
        $result = $db->loadResult();
        return $result;
    }

    /**
     * gets left cookie lifetime or sets fresh cookie
     * @param type $module - id for this content
     * @param type $lifetime - cookie lifetime
     * @return boolean|int
     */
    public static function getCookieModule($module,$lifetime) {
        $app = JFactory::getApplication();
        $jcookie = $app->input->cookie;
        $cookie     = $jcookie->get( $module, null, $filter = 'int' );
        $actualTime = json_encode( time() );
        
        if( $cookie === NULL ) {
            $new = $jcookie->set($module, $actualTime, time() + $lifetime, $app->get('cookie_path', '/'), $app->get('cookie_domain'), $app->isSSLConnection());
            return false;
        }
        else {
            $value = json_decode($cookie);
            if (is_numeric($value)) {
                $left = $lifetime - (time() - $value);
            } else {
                $left = 0;
            }
            return $left;
        }
    }
}